# floodgate-api

[OpenAPI](https://swagger.io/specification/) specification for the `floodgate` DEX API server + client development libraries & examples

## authentication

Every `floodgate` API request needs to be authenticated. We use a scheme that is analogous to the [FTX API authentication](https://blog.ftx.com/blog/api-authentication/) i.e. the following headers must be present:
   - `X-FG-KEY`: the public key of the solana wallet passed as a base-58 encoded string
   - `X-FG-TS`: a time stamp (number of milliseconds since Unix epoch), must not be older than 5 minutes
   - `X-FG-SIGN`: a [`ed25519` signature](https://docs.solana.com/developing/programming-model/transactions#signature-format) (in [hexadecimal encoding](https://pkg.go.dev/encoding/hex)) of the following strings:
      1. request timestamp (same as in `X-FG-TS`)
      1. HTTP method in uppercase (e.g. GET or POST)
      1. request path, including leading slash and any URL parameters but not including the hostname (e.g. /account)
      1. request body (JSON-encoded) only for POST requests
